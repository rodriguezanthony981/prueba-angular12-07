export const environment = {
  production: false,
  apiUrl: 'https://poetrydb.org',
  apiKey: 'your-dev-api-key'
};