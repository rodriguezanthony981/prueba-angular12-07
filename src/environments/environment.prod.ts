export const environment = {
  production: true,
  apiUrl: 'https://poetrydb.org',
  apiKey: 'your-prod-api-key'
};