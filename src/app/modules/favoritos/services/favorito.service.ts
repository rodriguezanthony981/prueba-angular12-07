import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { DialogData } from '../../autores/interfaces/autores.interface';

@Injectable({
  providedIn: 'root'
})
export class FavoritoService {
  favoritos: DialogData[] = [];

  constructor(private cookieService: CookieService) {
    this.recuperarFavoritos();
  }

  agregarFavorito(favorito: DialogData): boolean {
    const index = this.favoritos.findIndex(f => f.name === favorito.name);
    if (index === -1) {
      this.favoritos.push(favorito);
      this.actualizarCookie();
      return true;
    } else {
      return false;
    }
  }

  eliminarFavorito(name: string) {
    const index = this.favoritos.findIndex(f => f.name === name);
    if (index !== -1) {
      this.favoritos.splice(index, 1);
    }
    this.actualizarCookie();
  }

  obtenerFavoritos(): DialogData[]{
    this.recuperarFavoritos();
    return this.favoritos;
  }

  private actualizarCookie() {
    this.cookieService.set('favoritos', JSON.stringify(this.favoritos));
  }
  
  private recuperarFavoritos():void {
    const cookieValue = this.cookieService.get('favoritos');
    if(cookieValue) {
      this.favoritos = JSON.parse(cookieValue);
    }
  }
}
