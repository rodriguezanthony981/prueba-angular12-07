import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritasComponent } from './pages/main/favoritos.component';
import { MaterialModule } from '../material/material.module';
import { FavoritosRoutingModule } from './favoritos-routing.module';



@NgModule({
  declarations: [
    FavoritasComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FavoritosRoutingModule
  ]
})
export class FavoritosModule { }
