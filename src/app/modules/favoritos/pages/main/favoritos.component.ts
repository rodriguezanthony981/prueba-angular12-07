import { Component, OnInit } from '@angular/core';
import { FavoritoService } from '../../services/favorito.service';
import { DialogData } from 'src/app/modules/autores/interfaces/autores.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'obras-favoritas',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.scss']
})
export class FavoritasComponent implements OnInit{
  poemasFavoritos: DialogData[]=[]

  colapse = '' 

  constructor(private FavoritoService: FavoritoService, private router: Router){
    this.poemasFavoritos = FavoritoService.obtenerFavoritos();
  }
  ngOnInit(): void {
    
  }

  removeFavorito(name:string):void {
    this.FavoritoService.eliminarFavorito(name);
  }

  volver(): void{
    this.router.navigate(['/autores']);
  }
}
