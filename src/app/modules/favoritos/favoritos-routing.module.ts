import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoritasComponent } from './pages/main/favoritos.component';


const routes: Routes = [
  {
    path: '',
    component: FavoritasComponent
  },
  {
    path: '**', redirectTo: ''
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class FavoritosRoutingModule { }
