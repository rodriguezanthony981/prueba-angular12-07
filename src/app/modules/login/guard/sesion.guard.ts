import { inject } from '@angular/core';
import { CanMatchFn, Router } from '@angular/router';
import { LoginService } from '../services/login.service';

export const sesionGuard: CanMatchFn = (route, segments) => {

  let pass: boolean = false;
  const routed = inject(Router);
  const session = inject(LoginService);
  pass = session.verificaAuthentication();
  if(pass){
    console.log('Permitido sesión iniciada --Guard');
  } else {
    console.log('Bloqueado no tienes sesión --Guard');
    routed.navigate(['login']);
  }
  
  return pass;
};
