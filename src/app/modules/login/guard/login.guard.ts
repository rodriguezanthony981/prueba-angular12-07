import { inject } from '@angular/core';
import { CanMatchFn, Router } from '@angular/router';
import { LoginService } from '../services/login.service';


export const loginGuard: CanMatchFn = (route, segments) => {
  
  let pass: boolean = true;
  const routed = inject(Router);
  const session = inject(LoginService);
  pass = session.verificaAuthentication();  
  if(pass){
    console.log('Saltado login --Guard');
    routed.navigate(['autores']);
  } else {
    console.log('Llena el login --Guard');
  }
  return true
};
