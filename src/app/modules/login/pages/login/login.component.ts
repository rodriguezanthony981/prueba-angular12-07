import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  myForm: FormGroup = new FormGroup({
    user: new FormControl('',[Validators.required]),
    password: new FormControl('',[Validators.required])
  })

  constructor(private _route: Router, private _snackBar: MatSnackBar,
    private LoginService: LoginService) { 
  }

  onSubmit(): void{
      if(this.myForm.value.user === 'admin' && this.myForm.value.password === 'admin') {
        this._snackBar.open('Acceso Concedido', 'Cerrar',{
          duration: 3000,
        });
        this.LoginService.login();
        this._route.navigate(['autores']);
      } else {
        this._snackBar.open('Credenciales incorrectas', 'Cerrar',{
          duration: 3000,
        });
      }
  }
  borrar(){
    this.myForm.reset()
  }
}
