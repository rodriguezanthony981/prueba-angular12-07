import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _auth!: any;

  constructor(private cookieService: CookieService) { }

  verificaAuthentication(): boolean {
    const token = this.cookieService.get('token');
    if (token?.includes('admin')) {
      return true;
    } else {
      return false;
    }
  }

  login(): void {
    this.cookieService.set('token', 'admin');
  }

  loggout(): void {
    this.cookieService.delete('token');
  }
}
