import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaAutoresComponent } from './pages/main/lista.component';
import { MaterialModule } from '../material/material.module';
import { AutoresRoutingModule } from './autores-routing.module';

import { NavbarComponent } from './components/navbar/navbar.component';
import { ModalComponent } from './components/modal/modal.component';
import { AutoresTablaComponent } from './components/autores-tabla/autores-tabla.component';



@NgModule({
  declarations: [
    ListaAutoresComponent,
    NavbarComponent,
    ModalComponent,
    AutoresTablaComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AutoresRoutingModule,
  ],
})
export class AutoresModule { }
