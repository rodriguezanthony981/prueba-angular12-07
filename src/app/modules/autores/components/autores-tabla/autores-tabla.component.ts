import { Component, OnInit } from '@angular/core';
import { AutoresService } from '../../services/autores.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { FavoritoService } from 'src/app/modules/favoritos/services/favorito.service';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-autores-tabla',
  templateUrl: './autores-tabla.component.html',
  styles: [`
    .header-image {
      background-image: url('../../../../../assets/img/Artboard.svg');
      background-size: cover;
      background-position: center;
    }
  `]
})
export class AutoresTablaComponent implements OnInit{
  
  data!: string[];
  
  favorito!: string;
  name!: string;
  lines!:string;

  constructor(
    private autoresService: AutoresService, 
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private favoritoService: FavoritoService
  ) {  }
  
  ngOnInit() { 
    this.autoresService.getAutores()
      .subscribe({
        next: ({authors}) => {
          return this.data = authors
        },
        error: (err) => console.log(err)
      })
    }
  
    agregarFavorito(favorito: boolean, name: string, lines: string): boolean {
      return this.favoritoService.agregarFavorito({ favorito, name, lines });
    }
  
    eliminarFavorito() {
      this.favoritoService.eliminarFavorito('Objeto 1');
    }

  openDialog(author:string): void {
    this.name = author;
    const dialogRef = this.dialog.open( ModalComponent, {
      data: { name: this.name, favorito: this.favorito, lines: this.lines },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');      
      let status: boolean = false;
      let label!: string;
      if(result){
        status = this.agregarFavorito(result.favorito, result.name, result.lines);
        if(status){
          label = 'Poema Agregado a Favoritos';
        } else {
          label = 'El Poema ya estaba Agregado en Favoritos';
        }
        this._snackBar.open(label, 'Cerrar',{
          duration: 3000,
          panelClass: 'snackbar-custom'
        });
      }
    });
  }
}
