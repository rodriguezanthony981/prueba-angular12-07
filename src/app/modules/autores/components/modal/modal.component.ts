import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AutoresService } from '../../services/autores.service';
import { DialogData, obras } from '../../interfaces/autores.interface';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit{
  
  obras!: obras[];
  obraTitle!: string;
  poemaContent: string = '';
  loading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private autorService: AutoresService
  ) {}
  
  ngOnInit() {
    this.autorService.getObras(this.data.name)
      .subscribe({
        next: (res) => {
          return this.obras = res;
        }
      })
  } 

  onNoClick(): void {
    this.dialogRef.close();
  }

  buscarPoema(termino: string){
    this.loading = true;
    this.autorService.getTitle(termino)
      .subscribe({
        next: res => {
          this.obraTitle = res[0].title;
          this.poemaContent = res[0].lines;
          this.data.name = this.obraTitle;
          this.data.favorito = true;
          this.data.lines = this.poemaContent; 
          this.loading = false;
          return this.poemaContent;
        }
      })
  }
}
