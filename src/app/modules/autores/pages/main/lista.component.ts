import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { destacado } from '../../interfaces/autores.interface';
import { AutoresService } from '../../services/autores.service';
import { LoginService } from 'src/app/modules/login/services/login.service';


@Component({
  selector: 'lista-autores',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaAutoresComponent {
  data!: destacado[];
  Is: boolean = false;

  constructor(
    private autoresService: AutoresService,
    private router: Router,
    private LoginService: LoginService
  ) {  }

  ngOnInit() {
    this.autoresService.getDestacados()
      .subscribe({
        next: (res) => this.data = (res)
      });
  }

  change(){
    this.Is= !this.Is;
  }
  onMiVariableChange(nuevoValor: boolean) {
    this.change();
  }
  favoritos(){
    this.router.navigate(['/favoritos']);
  }

  cerrarSesion(){
    this.LoginService.loggout();
    this.router.navigate(['/login']);
  }
}
