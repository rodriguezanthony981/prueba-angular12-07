import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaAutoresComponent } from './pages/main/lista.component';



const routes: Routes = [
  {
    path: 'lista',
    component: ListaAutoresComponent
  },
  {
    path: '**', redirectTo: 'lista'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class AutoresRoutingModule { }
