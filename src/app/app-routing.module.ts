import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { loginGuard } from './modules/login/guard/login.guard';
import { sesionGuard } from './modules/login/guard/sesion.guard';


const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule),
    canMatch: [loginGuard],
  },
  {
    path: 'autores',
    loadChildren: () => import('./modules/autores/autores.module').then(m => m.AutoresModule),
    canMatch: [sesionGuard]
  },
  {
    path: 'favoritos',
    loadChildren: () => import('./modules/favoritos/favoritos.module').then(m => m.FavoritosModule),
    canMatch: [sesionGuard]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
